**Predicting the Direction of Tehran's Stock Market Price Using Machine Learning Models**  
  
**Usage:**  
Download the project.  
Then in the project directory, create a virtual environment using anaconda prompt and the following command:  
    `conda create -n py39 python=3.9 -y`  
Activate the environment using this command:  
    `conda activate py39`  
Install the required libraries from the requirements.txt file with the following command:  
    `pip install -r requirements.txt`  
Everything is ready to run the project.  
Open Jupyter Notebook using this command in the virtual environment and anaconda prompt:  
    `jupyter notebook`  
When Jupyter is launched, open the project's notebook and simply run the cells respectively.  
  
Contact nika.mosayebi@yahoo.com for more help.  
